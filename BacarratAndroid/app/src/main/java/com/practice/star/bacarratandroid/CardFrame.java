package com.practice.star.bacarratandroid;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Created by User on 30/10/2017.
 */

public class CardFrame extends FrameLayout {
    private static final String TAG = "starLog";

    private ImageView cardView;

    public CardFrame(@NonNull Context context) {
        super(context);
        init();
    }

    public CardFrame(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CardFrame(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        cardView = new Card(getContext());

        cardView.setAdjustViewBounds(true);
        cardView.setScaleType(ImageView.ScaleType.FIT_CENTER);

        LayoutParams lp = new LayoutParams(-1, -1);
        lp.setMargins(2,2,2,2);

        addView(cardView);
    }
}
