package com.practice.star.bacarratandroid;

/**
 * Created by User on 3/10/2017.
 */

public enum Result {
    Blank, Banker, Player, Tie
}
