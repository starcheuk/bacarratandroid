package com.practice.star.bacarratandroid;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;

/**
 * Created by User on 26/9/2017.
 */

public class MainRoad extends RoadBoard {
    private static final String TAG = "starLog";

    private int TieTimes = 0;

    public MainRoad(Context context) {
        super(context);
    }

    public MainRoad(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void BankerBtnPressed(Tile[][] mainRoadTile, Point mainRoadPoint) {
        TieTimes = 0;
        boolean predictorMode = false;
        Result result = Result.Banker;
        Point newPillPoint = GetNewPillPoint(result);
        CreatePill(result, newPillPoint, predictorMode);
    }

    @Override
    public void BankerPredictorBtnPressed(Tile[][] mainRoadTile, Point mainRoadPoint) {
        boolean predictorMode = true;
        Result result = Result.Banker;
        Point newPillPoint = GetNewPillPoint(result);
        CreatePill(result, newPillPoint, predictorMode);
    }

    @Override
    public void PlayerBtnPressed(Tile[][] mainRoadTile, Point mainRoadPoint) {
        TieTimes = 0;
        boolean predictorMode = false;
        Result result = Result.Player;
        Point newPillPoint = GetNewPillPoint(result);
        CreatePill(result, newPillPoint, predictorMode);
    }

    @Override
    public void PlayerPredictorBtnPressed(Tile[][] mainRoadTile, Point mainRoadPoint) {
        boolean predictorMode = true;
        Result result = Result.Player;
        Point newPillPoint = GetNewPillPoint(result);
        CreatePill(result, newPillPoint, predictorMode);
    }

    public void TieBtnPressed(){
        if(!PreviousResult.equals(Result.Blank)){
            TieTimes++;
            GetTiles()[CurrentPoint.x][CurrentPoint.y].SetTie(TieTimes);
        }
    }
}
