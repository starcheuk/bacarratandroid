package com.practice.star.bacarratandroid;

import android.graphics.Bitmap;
import android.graphics.Point;

/**
 * Created by User on 13/10/2017.
 */

public class ImageSplitter {

    public static ImagePiece[][] split(Bitmap bitmap, int xPiece, int yPiece){

        ImagePiece[][] pieces = new ImagePiece[xPiece][yPiece];
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int pieceWidth = width / xPiece;
        int pieceHeight = height / yPiece;
        for (int y = 0; y < yPiece; y++){
            for (int x = 0; x < xPiece; x++){

                ImagePiece piece = new ImagePiece();
                piece.index = new Point(x,y);
                int xValue = x * pieceWidth;
                int yValue = y * pieceHeight;
                piece.bitmap = Bitmap.createBitmap(bitmap, xValue, yValue, pieceWidth, pieceHeight);
                pieces[x][y] = piece;
            }
        }

        return  pieces;
    }
}
