package com.practice.star.bacarratandroid;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.support.annotation.NonNull;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Created by User on 21/9/2017.
 */



public class Tile extends FrameLayout {
    private static final String TAG = "starLog";

    public Result result;
    public ImageView TileImage;
    public boolean PredictorHasChoice;

    public Tile(@NonNull Context context) {
        super(context);

        init();
    }

    public void init(){
        TileImage = new ImageView(getContext());

        TileImage.setImageResource(R.drawable.blank);
        TileImage.setAdjustViewBounds(true);
        TileImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
        TileImage.setBackgroundColor(0x33ffffff);

        LayoutParams lp = new LayoutParams(-1, -1);
        lp.setMargins(1,1,1,1);
        addView(TileImage, lp);

        SetResult(Result.Blank , false);
    }

    private Activity getActivity(){
        Context context = getContext();
        while (context instanceof ContextWrapper){
            if(context instanceof Activity){
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    public Result GetResult(){
        return result;
    }

    public void SetResult(Result result, boolean predictorMode){

        if(!predictorMode){
            this.result = result;
            switch (result){
                case Blank:
                    break;
                case Banker:
                    PredictorHasChoice = true;
                    TileImage.clearAnimation();
                    TileImage.setImageResource(R.drawable.banker);
                    break;
                case Player:
                    PredictorHasChoice = true;
                    TileImage.clearAnimation();
                    TileImage.setImageResource(R.drawable.player);
                    break;
            }
        }else {
            switch (result){
                case Banker:
                    TileImage.setImageResource(R.drawable.banker);
                    SetPillAnim();
                    break;
                case Player:
                    TileImage.setImageResource(R.drawable.player);
                    SetPillAnim();
                    break;
            }
        }

    }

    public void SetResultManual(Result result, boolean predictorMode, boolean bankerPair, boolean playerPair) {
        if(!predictorMode){
            result = result;
            switch (result){
                case Blank:
                    break;
                case Banker:
                    PredictorHasChoice = true;
                    TileImage.clearAnimation();
                    TileImage.setImageResource(R.drawable.banker_manual);
                    CheckPair(bankerPair,playerPair);
                    break;
                case Player:
                    PredictorHasChoice = true;
                    TileImage.clearAnimation();
                    TileImage.setImageResource(R.drawable.player_manual);
                    CheckPair(bankerPair,playerPair);
                    break;
                case Tie:
                    PredictorHasChoice = true;
                    TileImage.clearAnimation();
                    TileImage.setImageResource(R.drawable.tie);
                    CheckPair(bankerPair,playerPair);
                    break;
            }
        }else {
            switch (result){
                case Banker:
                    TileImage.setImageResource(R.drawable.banker_manual);
                    SetPillAnim();
                    break;
                case Player:
                    TileImage.setImageResource(R.drawable.player_manual);
                    SetPillAnim();
                    break;
            }
        }
    }

    private void CheckPair(boolean bankerPair, boolean playerPair){
        if(bankerPair){
            ImageView view = new ImageView(getContext());
            view.setImageResource(R.drawable.banker_pair);
            addView(view);
        }
        if(playerPair){
            ImageView view = new ImageView(getContext());
            view.setImageResource(R.drawable.player_pair);
            addView(view);
        }
    }

    public void SetPillAnim(){

        AlphaAnimation alphaAnimation = new AlphaAnimation(0.1f, 1.0f);
        alphaAnimation.setDuration(200);
        TileImage.clearAnimation();
        TileImage.setAnimation(alphaAnimation);
        alphaAnimation.setRepeatCount(10);
        alphaAnimation.setRepeatMode(Animation.REVERSE);

        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Activity mainActivity = getActivity();
                if(mainActivity != null){
                    mainActivity.findViewById(R.id.BankerBtn).setEnabled(false);
                    mainActivity.findViewById(R.id.PlayerBtn).setEnabled(false);
                    mainActivity.findViewById(R.id.TieBtn).setEnabled(false);
                    mainActivity.findViewById(R.id.BankerPredictor).setEnabled(false);
                    mainActivity.findViewById(R.id.PlayerPredictor).setEnabled(false);
                }

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(!PredictorHasChoice){
                    TileImage.setImageResource(R.drawable.blank);
                    TileImage.clearAnimation();

                    Activity mainActivity = getActivity();
                    if(mainActivity != null){
                        mainActivity.findViewById(R.id.BankerBtn).setEnabled(true);
                        mainActivity.findViewById(R.id.PlayerBtn).setEnabled(true);
                        mainActivity.findViewById(R.id.TieBtn).setEnabled(true);
                        mainActivity.findViewById(R.id.BankerPredictor).setEnabled(true);
                        mainActivity.findViewById(R.id.PlayerPredictor).setEnabled(true);
                    }

                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        alphaAnimation.startNow();
    }

    public void SetTie(int tieTimes){
        if (tieTimes<=4){
            ImageView tieImage = new ImageView(getContext());

            switch (tieTimes){
                case 1:
                    tieImage.setImageResource(R.drawable.tie1);
                    break;
                case 2:
                    tieImage.setImageResource(R.drawable.tie2);
                    break;
                case 3:
                    tieImage.setImageResource(R.drawable.tie3);
                    break;
                case 4:
                    tieImage.setImageResource(R.drawable.tie4);
                    break;
            }

            addView(tieImage);
        }
    }

}
