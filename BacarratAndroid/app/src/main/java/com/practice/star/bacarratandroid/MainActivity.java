package com.practice.star.bacarratandroid;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "starLog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getSupportActionBar() != null){
            getSupportActionBar().hide();
        }

        Button bankerBtn, playerBtn, tieBtn, bankerPredictor, playerPredictor;

        bankerBtn = (Button)findViewById(R.id.BankerBtn);
        playerBtn = (Button)findViewById(R.id.PlayerBtn);
        tieBtn = (Button)findViewById(R.id.TieBtn);
        bankerPredictor = (Button)findViewById(R.id.BankerPredictor);
        playerPredictor = (Button)findViewById(R.id.PlayerPredictor);

        bankerBtn.setOnClickListener(ButtonListener);
        playerBtn.setOnClickListener(ButtonListener);
        tieBtn.setOnClickListener(ButtonListener);
        bankerPredictor.setOnClickListener(ButtonListener);
        playerPredictor.setOnClickListener(ButtonListener);

    }

    private Button.OnClickListener ButtonListener = new Button.OnClickListener(){
        @Override
        public void onClick(View view) {
            MainRoad mainRoad = (MainRoad)findViewById(R.id.MainRoad);
            RoadBoard BigEyeRoad = (RoadBoard)findViewById(R.id.BigEyeRoad);
            RoadBoard SmallRoad = (RoadBoard)findViewById(R.id.SmallRoad);
            RoadBoard CockroachRoad = (RoadBoard)findViewById(R.id.CockroachRoad);
            ManualRoad manualRoad = (ManualRoad)findViewById(R.id.ManualRoad);

            CheckBox checkBox = (CheckBox)findViewById(R.id.BankerPair);
            boolean bankerPairChecked = checkBox.isChecked();
            checkBox = (CheckBox)findViewById(R.id.PlayerPair);
            boolean playerPairChecked = checkBox.isChecked();

            Tile[][] mainRoadTile;
            Point mainRoadCurrentPoint;

            switch (view.getId()){
                case R.id.BankerBtn:
                    mainRoad.BankerBtnPressed(null, new Point(0,0));

                    mainRoadTile = mainRoad.GetTiles();
                    mainRoadCurrentPoint = mainRoad.getCurrentPoint();

                    BigEyeRoad.BankerBtnPressed(mainRoadTile,mainRoadCurrentPoint);
                    SmallRoad.BankerBtnPressed(mainRoadTile,mainRoadCurrentPoint);
                    CockroachRoad.BankerBtnPressed(mainRoadTile,mainRoadCurrentPoint);
                    manualRoad.BankerBtnPressed(bankerPairChecked, playerPairChecked);
                    break;

                case R.id.PlayerBtn:
                    mainRoad.PlayerBtnPressed(null, new Point(0,0));

                    mainRoadTile = mainRoad.GetTiles();
                    mainRoadCurrentPoint = mainRoad.getCurrentPoint();

                    BigEyeRoad.PlayerBtnPressed(mainRoadTile,mainRoadCurrentPoint);
                    SmallRoad.PlayerBtnPressed(mainRoadTile,mainRoadCurrentPoint);
                    CockroachRoad.PlayerBtnPressed(mainRoadTile,mainRoadCurrentPoint);
                    manualRoad.PlayerBtnPressed(bankerPairChecked, playerPairChecked);
                    break;

                case R.id.TieBtn:
                    mainRoad.TieBtnPressed();
                    manualRoad.TieBtnPressed(bankerPairChecked, playerPairChecked);
                    break;

                case R.id.BankerPredictor:
                    mainRoad.BankerPredictorBtnPressed(null, new Point(0,0));

                    mainRoadTile = mainRoad.GetTiles();
                    mainRoadCurrentPoint = mainRoad.getCurrentPoint();

                    BigEyeRoad.BankerPredictorBtnPressed(mainRoadTile,mainRoadCurrentPoint);
                    SmallRoad.BankerPredictorBtnPressed(mainRoadTile,mainRoadCurrentPoint);
                    CockroachRoad.BankerPredictorBtnPressed(mainRoadTile,mainRoadCurrentPoint);
                    manualRoad.BankerPredictorBtnPressed();
                    break;

                case R.id.PlayerPredictor:
                    mainRoad.PlayerPredictorBtnPressed(null, new Point(0,0));

                    mainRoadTile = mainRoad.GetTiles();
                    mainRoadCurrentPoint = mainRoad.getCurrentPoint();

                    BigEyeRoad.PlayerPredictorBtnPressed(mainRoadTile,mainRoadCurrentPoint);
                    SmallRoad.PlayerPredictorBtnPressed(mainRoadTile,mainRoadCurrentPoint);
                    CockroachRoad.PlayerPredictorBtnPressed(mainRoadTile,mainRoadCurrentPoint);
                    manualRoad.PlayerPredictorBtnPressed();
                    break;
            }
        }
    };
}
