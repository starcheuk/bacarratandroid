package com.practice.star.bacarratandroid;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Created by User on 30/10/2017.
 */

public class ResultCard extends FrameLayout {
    private static final String TAG = "starLog";

    private ImagePiece[][] CardList;
    private Bitmap CardBack, CardFront;

    private ImageView cardView;

    public ResultCard(@NonNull Context context) {
        super(context);
        init();
    }

    public ResultCard(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ResultCard(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
//        CardList = ImageSplitter.split(BitmapFactory.decodeResource(getResources(), R.drawable.cards), 13, 5);
//        CardBack = CardList[0][4].bitmap;
//        CardFront = CardList[0][0].bitmap;
//
//        cardView = new ImageView(getContext());
//        cardView.setImageBitmap(CardBack);
//
//        cardView.setAdjustViewBounds(true);
//        cardView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//
//        LayoutParams lp = new LayoutParams(-1, -1);
//        lp.setMargins(2,2,2,2);
//
//        addView(cardView);
    }
}
