package com.practice.star.bacarratandroid;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;

/**
 * Created by User on 27/9/2017.
 */

public class ManualRoad extends RoadBoard {
    private static final String TAG = "starLog";

    public ManualRoad(Context context) {
        super(context);
    }

    public ManualRoad(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void CreatePill(Result result, boolean predictorMode, boolean bankerPair, boolean playerPair) {
        Tiles[CurrentPoint.x][CurrentPoint.y].SetResultManual(result,predictorMode,bankerPair,playerPair);

        if(!predictorMode){
            if(CurrentPoint.y >= 5 ){
                CurrentPoint = new Point(CurrentPoint.x + 1, 0);
            }else {
                CurrentPoint.y++;
            }
        }
    }

    public void BankerBtnPressed(boolean bankerPair, boolean playerPair) {
        Result result = Result.Banker;
        boolean predictorMode = false;
        CreatePill(result, predictorMode, bankerPair, playerPair);
    }

    public void BankerPredictorBtnPressed() {
        Result result = Result.Banker;
        boolean predictorMode = true;
        CreatePill(result, predictorMode, false, false);
    }

    public void PlayerBtnPressed(boolean bankerPair, boolean playerPair) {
        Result result = Result.Player;
        boolean predictorMode = false;
        CreatePill(result, predictorMode, bankerPair, playerPair);
    }

    public void PlayerPredictorBtnPressed() {
        Result result = Result.Player;
        boolean predictorMode = true;
        CreatePill(result, predictorMode, false, false);
    }

    public void TieBtnPressed(boolean bankerPair, boolean playerPair){
        Result result = Result.Tie;
        boolean predictorMode = false;
        CreatePill(result, predictorMode, bankerPair, playerPair);
    }

}
