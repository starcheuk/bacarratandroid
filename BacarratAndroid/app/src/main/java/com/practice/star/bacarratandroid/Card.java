package com.practice.star.bacarratandroid;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Region;
import android.os.Build;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by User on 18/10/2017.
 */

public class Card extends AppCompatImageView {
    private static final String TAG = "starLog";

    private int cardWidth, cardHeight;
    private ImagePiece[][] CardList;
    private Bitmap CardBack, CardFront, CardBackOrigin, CardFrontOrigin;

    private Path path0, path1;
    private Paint paint;

    private PointF touchPoint = new PointF();
    private PointF startTouchPoint = new PointF();

    private PointF bezierControl1 = new PointF();
    private PointF bezierControl2 = new PointF();

    private PointF frontCardPivot = new PointF();

    private int viewWidth = 0;
    private int viewHeight = 0;
    private int cornerX = 0;
    private int cornerY = 0;
    private float middleX = 0;
    private float middleY = 0;
    private int originX = 0;
    private int originY = 0;
    private int cornerSize = 0;

    private float theta;

    Matrix matrix;

    private boolean isDragging;
    private boolean isOpened;

    private enum TouchArea{ blank, top, bottom, left, right, topLeft, topRight, bottomLeft, bottomRight};
    private TouchArea ta = TouchArea.blank;

    public Card(Context context) {
        super(context);
        init();
    }

    public Card(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Card(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        CardList = ImageSplitter.split(BitmapFactory.decodeResource(getResources(), R.drawable.cards), 13, 5);
        CardBackOrigin = CardList[0][4].bitmap;
        CardFrontOrigin = CardList[0][0].bitmap;

        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);

        matrix = new Matrix();

        touchPoint.x = 0.01f;
        touchPoint.y = 0.01f;

        path0 = new Path();
        path1 = new Path();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        viewWidth = w;
        viewHeight = h;
        adjustCardSize();
    }

    private void adjustCardSize(){
        Matrix matrix = new Matrix();

        int cardViewSize = (viewWidth <= viewHeight) ? viewWidth : viewHeight;
        cardWidth = cardViewSize / 2;
        cardHeight = (int)((float)CardBackOrigin.getHeight() / (float)CardBackOrigin.getWidth() * cardWidth);

        matrix.postScale((float)cardWidth / (float)CardBackOrigin.getWidth(), (float)cardHeight / (float)CardBackOrigin.getHeight());
        CardBack = Bitmap.createBitmap(CardBackOrigin, 0, 0 , CardBackOrigin.getWidth(), CardBackOrigin.getHeight(), matrix, false);
        CardFront = Bitmap.createBitmap(CardFrontOrigin, 0, 0, CardFrontOrigin.getWidth(), CardFrontOrigin.getHeight(), matrix, false);

        originX = (viewWidth / 2) - (cardWidth / 2);
        originY = (viewHeight / 2) - (cardHeight / 2);

        cornerSize = (cardWidth / 4 < cardHeight / 4) ? cardWidth / 4 : cardHeight / 4;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (isOpened){
            canvas.drawBitmap(CardFront, originX, originY, null);
        }else if (ta == TouchArea.blank){
            canvas.drawBitmap(CardBack, originX, originY, null);
        } else {
            calculatePoints();
            drawCurrentPageArea(canvas, CardBack);
            drawCurrentBackArea(canvas, CardFront);
        }
    }

    private void calculatePoints(){
        getPoints();

        switch (ta){
            case bottomLeft:
                if (touchPoint.x <= originX){
                    touchPoint.x = 0.01f + originX;
                    getPoints();
                }
                if (touchPoint.y >= cardHeight + originY){
                    touchPoint.y = cardHeight + originY - 0.01f;
                    getPoints();
                }
                break;
            case bottomRight:
                if (touchPoint.x >= cardWidth + originX){
                    touchPoint.x = cardWidth + originX - 0.01f;
                    getPoints();
                }
                if (touchPoint.y >= cardHeight + originY){
                    touchPoint.y = cardHeight + originY - 0.01f;
                    getPoints();
                }
                break;
            case topLeft:
                if (touchPoint.x <= originX){
                    touchPoint.x = 0.01f + originX;
                    getPoints();
                }
                if (touchPoint.y <= originY){
                    touchPoint.y = originY + 0.01f;
                    getPoints();
                }
                break;
            case topRight:
                if (touchPoint.x >= cardWidth + originX){
                    touchPoint.x = cardWidth + originX - 0.01f;
                    getPoints();
                }
                if (touchPoint.y <= originY){
                    touchPoint.y = originY + 0.01f;
                    getPoints();
                }
                break;
            case bottom:
                touchPoint.x = originX + 0.01f;
                getPoints();
                break;
            case top:
                touchPoint.x = originX + 0.01f;
                getPoints();
                break;
            case left:
                touchPoint.y = originY + cardHeight - 0.01f;
                getPoints();
                break;
            case right:
                touchPoint.y = originY + cardHeight - 0.01f;
                getPoints();
                break;
        }
    }

    private void getPoints(){
        middleX = (touchPoint.x + cornerX) / 2;
        middleY = (touchPoint.y + cornerY) / 2;

        bezierControl1.x = middleX - (cornerY - middleY) * (cornerY - middleY) / (cornerX - middleX);
        bezierControl1.y = cornerY;

        bezierControl2.x = cornerX;
        bezierControl2.y = middleY - (cornerX - middleX) * (cornerX - middleX) / (cornerY - middleY);
    }

    private void drawCurrentPageArea(Canvas canvas, Bitmap bitmap){
        path0.reset();
        path0.moveTo(bezierControl1.x, bezierControl1.y);
        path0.lineTo(touchPoint.x, touchPoint.y);
        path0.lineTo(bezierControl2.x, bezierControl2.y);
        path0.lineTo(cornerX, cornerY);
        path0.close();

        canvas.save();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            canvas.clipOutPath(path0);
        }else {
            canvas.clipPath(path0, Region.Op.XOR);
        }
        canvas.drawBitmap(bitmap, originX, originY, null);
        canvas.restore();
    }

    private void drawCurrentBackArea(Canvas canvas, Bitmap bitmap){
        path1.reset();
        path1.moveTo(bezierControl1.x, bezierControl1.y);
        path1.lineTo(touchPoint.x, touchPoint.y);
        path1.lineTo(bezierControl2.x, bezierControl2.y);

        path1.close();

        canvas.save();
        canvas.clipPath(path1);

        switch (ta){
            case bottomLeft:
                frontCardPivot = new PointF(0, 0);
                theta = (float) Math.toDegrees(Math.atan2((touchPoint.y - cardHeight - originY) * -1, touchPoint.x - originX));
                break;
            case bottomRight:
                frontCardPivot = new PointF(1, 0);
                theta = (float) Math.toDegrees(Math.atan2((touchPoint.y - cardHeight - originY) * -1, touchPoint.x - cardWidth - originX));
                break;
            case topLeft:
                frontCardPivot = new PointF(0, 1);
                theta = (float) Math.toDegrees(Math.atan2((touchPoint.y - originY) * -1, touchPoint.x - originX));
                break;
            case topRight:
                frontCardPivot = new PointF(1, 1);
                theta = (float) Math.toDegrees(Math.atan2((touchPoint.y - originY) * -1, touchPoint.x - cardWidth - originX));
                break;
            case bottom:
                frontCardPivot = new PointF(0, 0);
                theta = (float) Math.toDegrees(Math.atan2((touchPoint.y - cardHeight - originY) * -1, touchPoint.x - originX));
                break;
            case top:
                frontCardPivot = new PointF(0, 1);
                theta = (float) Math.toDegrees(Math.atan2((touchPoint.y - originY) * -1, touchPoint.x - originX));
                break;
            case left:
                frontCardPivot = new PointF(0, 0);
                theta = (float) Math.toDegrees(Math.atan2((touchPoint.y - cardHeight - originY) * -1, touchPoint.x - originX));
                break;
            case right:
                frontCardPivot = new PointF(1, 0);
                theta = (float) Math.toDegrees(Math.atan2((touchPoint.y - cardHeight - originY) * -1, touchPoint.x - cardWidth - originX));
                break;
        }

        matrix.setTranslate(touchPoint.x - cardWidth * frontCardPivot.x, touchPoint.y - cardHeight * frontCardPivot.y);
        matrix.preRotate( (90.0f - theta) * 2.0f, cardWidth * frontCardPivot.x, cardHeight * frontCardPivot.y);

        canvas.drawBitmap(bitmap, matrix, paint);
        canvas.restore();
    }

    private void calculateCornerXY(float x, float y){
        if (x <= originX + cornerSize && y >= originY + cardHeight - cornerSize){
            ta = TouchArea.bottomLeft;
            cornerX = originX;
            cornerY = originY + cardHeight;
        }
        if (x >= originX + cardWidth - cornerSize && y >= originY + cardHeight - cornerSize){
            ta = TouchArea.bottomRight;
            cornerX = cardWidth + originX;
            cornerY = cardHeight + originY;
        }
        if(x <= originX + cornerSize && y <= originY + cornerSize){
            ta = TouchArea.topLeft;
            cornerX = originX;
            cornerY = originY;
        }
        if(x >= originX + cardWidth - cornerSize && y <= originY + cornerSize){
            ta = TouchArea.topRight;
            cornerX = cardWidth + originX;
            cornerY = originY;
        }
        if(x > originX + cornerSize && x < originX + cardWidth - cornerSize && y > originY + cardHeight - cornerSize){
            ta = TouchArea.bottom;
            cornerX = originX;
            cornerY = originY + cardHeight;
        }
        if(x > originX + cornerSize && x < originX + cardWidth - cornerSize && y < originY + cornerSize){
            ta = TouchArea.top;
            cornerX = originX;
            cornerY = originY;
        }
        if(x < originX + cornerSize && y > originY + cornerSize && y < originY + cardHeight - cornerSize){
            ta = TouchArea.left;
            cornerX = originX;
            cornerY = originY + cardHeight;
        }
        if(x > originX  + cardWidth - cornerSize && y > originY + cornerSize && y < originY + cardHeight - cornerSize){
            ta = TouchArea.right;
            cornerX = cardWidth + originX;
            cornerY = cardHeight + originY;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        final int x = (int)event.getX();
        final int y = (int)event.getY();

        switch (event.getAction() & MotionEvent.ACTION_MASK){
            case MotionEvent.ACTION_DOWN:
                if(!isDragging && ! isOpened){
                    touchPoint.x = x;
                    touchPoint.y = y;
                    startTouchPoint.x = x;
                    startTouchPoint.y = y;
                    isDragging = true;
                    calculateCornerXY(x, y);
                    invalidate();
                }
                break;
            case MotionEvent.ACTION_UP:
                isDragging = false;
                ta = TouchArea.blank;
                touchPoint.x = cornerX;
                touchPoint.y = cornerY;
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                if(isDragging && ta != TouchArea.blank && ! isOpened){
                    touchPoint.x = x;
                    touchPoint.y = y;
                    float dis = (float)Math.sqrt(Math.pow((double)(startTouchPoint.x - x) , 2) + Math.pow((double)(startTouchPoint.y - y) , 2));
                    if (dis > cardHeight)
                        isOpened = true;
                    invalidate();
                }
                break;
        }
        return true;
    }

}
