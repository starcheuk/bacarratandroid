package com.practice.star.bacarratandroid;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;

/**
 * Created by User on 25/9/2017.
 */

public class RoadBoard extends TableLayout {
    private static final String TAG = "starLog";

    public Tile[][] Tiles;
    private int xTile;

    private int xInit, yInit;

    public Result PreviousResult = Result.Blank;
    public Point CurrentPoint = new Point(0,0);
    private int PlusPointX = 0;
    private int LeadingX = 0;

    public RoadBoard(Context context) {
        super(context);
        init(null);
    }

    public RoadBoard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    private void init(AttributeSet attributeSet) {

        int xSize = 0;
        int ySize = 0;

        if(attributeSet != null){
            Resources.Theme theme = getContext().getTheme();
            TypedArray typedArray = theme.obtainStyledAttributes(attributeSet, R.styleable.RoadBoard,0,0);
            int count = typedArray.getIndexCount();
            for(int i = 0; i < count; i++){
                int styleAttr = typedArray.getIndex(i);
                switch (styleAttr){
                    case R.styleable.RoadBoard_x_init:
                        xInit = typedArray.getInt(i,0);
                        break;
                    case R.styleable.RoadBoard_y_init:
                        yInit = typedArray.getInt(i,0);
                        break;
                    case R.styleable.RoadBoard_x_size:
                        xSize = typedArray.getInt(i,0);
                        xTile = typedArray.getInt(i,0);
                        break;
                    case R.styleable.RoadBoard_y_size:
                        ySize = typedArray.getInt(i,0);
                }
            }
            typedArray.recycle();
        }

        BuildRoadBoard(xSize,ySize);
    }

    public void BuildRoadBoard(int xSize, int ySize){
        Tiles = new Tile[xSize][ySize];

        for(int y = 0; y < ySize; y++){
            TableRow row = new TableRow(getContext());
            for(int x = 0; x < xSize; x++){
                Tile tile = new Tile(getContext());
                tile.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT,1));
                row.addView(tile);

                Tiles[x][y] = tile;
            }
            addView(row);
        }
    }

    private Result GetPillNeedCreate(Tile[][] mainRoadTile, Point mainRoadPoint){
        // Checking for reach the init point
        if (!mainRoadTile[xInit + 1][0].GetResult().equals(Result.Blank) || !mainRoadTile[xInit][yInit].GetResult().equals(Result.Blank)){

            if(mainRoadPoint.y <= 0){ // If the first row of the column check for amount of each column and return the result
                int firstColumnAmount = 0;
                int secondColumnAmount = 0;

                for(int y = 0; y < 6; y++){
                    if(!mainRoadTile[mainRoadPoint.x-1][y].GetResult().equals(Result.Blank)){
                        firstColumnAmount++;
                    }
                    if(!mainRoadTile[mainRoadPoint.x - 1 - xInit][y].GetResult().equals(Result.Blank)){
                        secondColumnAmount++;
                    }
                }

                if(firstColumnAmount == secondColumnAmount){
                    return Result.Banker;
                }else {
                    return Result.Player;
                }
            }else {
                if(!mainRoadTile[mainRoadPoint.x - xInit][mainRoadPoint.y].GetResult().equals(Result.Blank)){
                    return Result.Banker;
                }else {
                    if(mainRoadTile[mainRoadPoint.x - xInit][mainRoadPoint.y - 1].GetResult().equals(Result.Blank)){
                        return Result.Banker;
                    }else {
                        return Result.Player;
                    }
                }
            }
        }else {
            return Result.Blank;
        }
    }

    public Point GetNewPillPoint(Result result){

        if(PreviousResult.equals(Result.Blank)){
            return new Point(0,0);
        }else if(!PreviousResult.equals(result)){
            Point point = new Point(0,0);
            for(int x = LeadingX; x < xTile; x++){
                if(!Tiles[x][0].GetResult().equals(Result.Blank)){
                    point = new Point(x + 1, 0);
                }
            }
            return point;
        }else {
            if(LeadingX == PlusPointX && CurrentPoint.y < 5 && Tiles[PlusPointX][CurrentPoint.y + 1].GetResult().equals(Result.Blank)){
                return new Point(PlusPointX, CurrentPoint.y + 1);
            }else {
                return new Point(PlusPointX + 1, CurrentPoint.y);
            }
        }
    }

    public void CreatePill(Result result, Point point, boolean predictorMode){
        Tiles[point.x][point.y].SetResult(result, predictorMode);

        if(!predictorMode){
            if(point.y <= 0){
                LeadingX = point.x;
                PlusPointX = LeadingX;
            }else {
                PlusPointX = point.x;
            }

            PreviousResult = result;
            CurrentPoint = point;
        }
    }

    public void BankerBtnPressed(Tile[][] mainRoadTile, Point mainRoadPoint){
        boolean predictorMode = false;
        Result result = GetPillNeedCreate(mainRoadTile, mainRoadPoint);
        if(!result.equals(Result.Blank)){
            Point newPillPoint = GetNewPillPoint(result);
            CreatePill(result,newPillPoint, predictorMode);
        }
    }

    public void BankerPredictorBtnPressed(Tile[][] mainRoadTile, Point mainRoadPoint){
        boolean predictorMode = true;
        Result result = GetPillNeedCreate(mainRoadTile, mainRoadPoint);
        if(!result.equals(Result.Blank)){
            Point newPillPoint = GetNewPillPoint(result);
            CreatePill(result,newPillPoint, predictorMode);
        }
    }

    public void PlayerBtnPressed(Tile[][] mainRoadTile, Point mainRoadPoint){
        boolean predictorMode = false;
        Result result = GetPillNeedCreate(mainRoadTile, mainRoadPoint);
        if(!result.equals(Result.Blank)){
            Point newPillPoint = GetNewPillPoint(result);
            CreatePill(result,newPillPoint, predictorMode);
        }
    }

    public void PlayerPredictorBtnPressed(Tile[][] mainRoadTile, Point mainRoadPoint){
        boolean predictorMode = true;
        Result result = GetPillNeedCreate(mainRoadTile, mainRoadPoint);
        if(!result.equals(Result.Blank)){
            Point newPillPoint = GetNewPillPoint(result);
            CreatePill(result,newPillPoint, predictorMode);
        }
    }

    public Tile[][] GetTiles(){
        return Tiles;
    }
    public Point getCurrentPoint(){
        return CurrentPoint;
    }
}
